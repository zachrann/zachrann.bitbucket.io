var task__motor_8py =
[
    [ "task_motor.TaskMotor", "classtask__motor_1_1TaskMotor.html", "classtask__motor_1_1TaskMotor" ],
    [ "DUTY", "task__motor_8py.html#af21ec8962dd301e72ab39d2605553854", null ],
    [ "E_N", "task__motor_8py.html#ab544ae149ca5d356e927803b2a1abedf", null ],
    [ "NO_FAULT", "task__motor_8py.html#a10c947209228b680d984ea5c0729ac2a", null ],
    [ "pinB0", "task__motor_8py.html#a2c71b045834df64dbc628d85db2bd62b", null ],
    [ "pinB1", "task__motor_8py.html#ae53a471752a2f4cf930422fd309ea862", null ],
    [ "pinB4", "task__motor_8py.html#aa0e4c695464b467938a1790e2ef39a3d", null ],
    [ "pinB5", "task__motor_8py.html#ac1d1e63b2a2fd1437857e6eb81aaf6eb", null ],
    [ "POS", "task__motor_8py.html#a36ee8bd3c0fd332ab1492ede36dbcd56", null ],
    [ "VEL", "task__motor_8py.html#a400789b2b2c4eb0dcc71652eb0a539dc", null ],
    [ "ZERO", "task__motor_8py.html#a0d6426ce3a8907d30be67e9ebd5ddfaf", null ]
];