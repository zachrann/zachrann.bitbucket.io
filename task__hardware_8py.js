var task__hardware_8py =
[
    [ "task_hardware.Task_Hardware", "classtask__hardware_1_1Task__Hardware.html", "classtask__hardware_1_1Task__Hardware" ],
    [ "CHECK_FAULT", "task__hardware_8py.html#ab0c77107d56d07732f3e68afbfcdb968", null ],
    [ "DUTY", "task__hardware_8py.html#ac8d48dd5e51006af980e9f396b71a3e7", null ],
    [ "E_N", "task__hardware_8py.html#a9f02b72b1e04ca5f6444519fed703f82", null ],
    [ "FIX_FAULT", "task__hardware_8py.html#a870f8e619171bbfa63e5a3a6c688b684", null ],
    [ "KP", "task__hardware_8py.html#a2b55454e0625b0a385e7aae440d28839", null ],
    [ "pinB0", "task__hardware_8py.html#a869b3f281d06d050aed77faee3267bba", null ],
    [ "pinB1", "task__hardware_8py.html#a620ec053193e879ad7da5c35261490af", null ],
    [ "pinB4", "task__hardware_8py.html#a8f5792fdbe15977812268f35a4513995", null ],
    [ "pinB5", "task__hardware_8py.html#a022e876bea3391d5f13cc51e9291295c", null ],
    [ "pinB6", "task__hardware_8py.html#a785600cbbaac22c03842f7092f2d2ccf", null ],
    [ "pinB7", "task__hardware_8py.html#a9e897909baf87c6d9db0bff270c20929", null ],
    [ "pinC6", "task__hardware_8py.html#a71dfebef23b27865c274cb596050ebad", null ],
    [ "pinC7", "task__hardware_8py.html#a5d5b20bce013fef59263ed82cbe5f21d", null ],
    [ "POS", "task__hardware_8py.html#af838997c383a30e53e08d001f3ae4e71", null ],
    [ "REF_POS", "task__hardware_8py.html#a361d03f34f78649e9cbe7305b6992814", null ],
    [ "REF_VEL", "task__hardware_8py.html#ac96f84a302ec5b9c9707c62d46ca995e", null ],
    [ "tick2rad", "task__hardware_8py.html#ae34bd5dbcc0b58f3c9452441c3929f55", null ],
    [ "VEL", "task__hardware_8py.html#a033e1fe68bb160d49bd00e7c0fcc62a8", null ],
    [ "ZERO", "task__hardware_8py.html#a4546973c540aa07ba4ec22d9ba377a7e", null ]
];