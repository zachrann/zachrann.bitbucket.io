var task__userLAB3_8py =
[
    [ "task_userLAB3.TaskUser", "classtask__userLAB3_1_1TaskUser.html", "classtask__userLAB3_1_1TaskUser" ],
    [ "CHECK_FAULT", "task__userLAB3_8py.html#a5996f49a8fe43c10ec4d266619ee5636", null ],
    [ "CommReader", "task__userLAB3_8py.html#a425d9966a3de2dcfd4ce2099207ba607", null ],
    [ "DUTY", "task__userLAB3_8py.html#a7a2d7910bc07d315aae88d7a976e06fb", null ],
    [ "EN", "task__userLAB3_8py.html#a4b75c47509c8ac8b7987a5ef32a2e050", null ],
    [ "FIX_FAULT", "task__userLAB3_8py.html#a2b651214a93331627cec9654b49282e9", null ],
    [ "KP", "task__userLAB3_8py.html#a15e2b01a0cbdeb6739d870bba63ee743", null ],
    [ "POS", "task__userLAB3_8py.html#a2c33b2cf14b606ac7ac881a01e38543e", null ],
    [ "Proportional_Prompt", "task__userLAB3_8py.html#ad1cbbb1873081cbbc4bde99b6908d09d", null ],
    [ "REF_POS", "task__userLAB3_8py.html#a987009f5e7eff7f1e5e7283a08512024", null ],
    [ "REF_VEL", "task__userLAB3_8py.html#a481c9a5c76736cb2759983b65bbfb8af", null ],
    [ "S0_INIT", "task__userLAB3_8py.html#aeac8f811e59cf85b2dedce258648f8c9", null ],
    [ "S1_WAIT_FOR_INPUT", "task__userLAB3_8py.html#a0b951d9ed72e3bc7e255c9107bdd01b9", null ],
    [ "S2_REQUEST", "task__userLAB3_8py.html#a07fe90bc70cf8818c08bc1338e904d8c", null ],
    [ "S3_WAIT", "task__userLAB3_8py.html#a4ec80c59975f5eab76ae77044d85d15c", null ],
    [ "VEL", "task__userLAB3_8py.html#a85692cf18e295aee6e3edf5920fcc93e", null ],
    [ "ZERO", "task__userLAB3_8py.html#a254e536a67637cf8071eff8f55ddb8f7", null ]
];