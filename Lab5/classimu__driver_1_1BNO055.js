var classimu__driver_1_1BNO055 =
[
    [ "__init__", "classimu__driver_1_1BNO055.html#ab41d6914a744a88b32564eba99e048f0", null ],
    [ "angularVel", "classimu__driver_1_1BNO055.html#ada06ef40f1310e603347ea8320e50efe", null ],
    [ "calibrate", "classimu__driver_1_1BNO055.html#ad1d19425fcf7e38a7d946fbb4fa094de", null ],
    [ "calibStatus", "classimu__driver_1_1BNO055.html#a631ef6616b5ad8dfc4f76f0d3a3ca349", null ],
    [ "deinit", "classimu__driver_1_1BNO055.html#a2d0c3df8c75587f1f4c350587495c9e5", null ],
    [ "eulerAngles", "classimu__driver_1_1BNO055.html#ae64b1917ae610e8d6e4bf2292c44b1d4", null ],
    [ "operatingMode", "classimu__driver_1_1BNO055.html#a7d8b6125c991cd8f971fb8e8380e7b60", null ],
    [ "readCalibCoeff", "classimu__driver_1_1BNO055.html#a398abf04c505ddde17ff28d71eb706d0", null ],
    [ "readOM", "classimu__driver_1_1BNO055.html#ab1190e4ce9c7b12e3dd08c2f3cd26ead", null ],
    [ "readwriteCalibBuf", "classimu__driver_1_1BNO055.html#af1101ec016b2af4b42dabd46168e2406", null ],
    [ "writeCalibCoeff", "classimu__driver_1_1BNO055.html#a8dd42af98a25df3a013c6bd24659e9fe", null ],
    [ "calibCoeffBuff", "classimu__driver_1_1BNO055.html#ae0fc30b31dfe46c311b964cc9771c672", null ],
    [ "eulerbuf", "classimu__driver_1_1BNO055.html#a975d6ab5f4905475afc62178fd9a2f62", null ],
    [ "gyroBuff", "classimu__driver_1_1BNO055.html#a5c1ab649a314ca439a3503a5f6a9c5d3", null ],
    [ "i2c", "classimu__driver_1_1BNO055.html#ab99dfaa0b501661cd70231fec2fb747a", null ]
];