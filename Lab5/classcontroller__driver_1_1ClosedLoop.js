var classcontroller__driver_1_1ClosedLoop =
[
    [ "__init__", "classcontroller__driver_1_1ClosedLoop.html#a83c0e1c2204fb9ec4867cb67208c6923", null ],
    [ "duty", "classcontroller__driver_1_1ClosedLoop.html#a95d65555e3b59a03563555aa0d39301d", null ],
    [ "get_Kp", "classcontroller__driver_1_1ClosedLoop.html#afe605ab8b5c13c07e310e949e893e80f", null ],
    [ "run", "classcontroller__driver_1_1ClosedLoop.html#af25ebbf17dfc10ffdf8fabe4b081bb3c", null ],
    [ "set_Kp", "classcontroller__driver_1_1ClosedLoop.html#a09c10fe2fd667238f327a477c6ae4681", null ],
    [ "Kp", "classcontroller__driver_1_1ClosedLoop.html#adbc3245ca10273e181d9a58d857b5248", null ],
    [ "last_error", "classcontroller__driver_1_1ClosedLoop.html#ad4dbf2939668e8e7f91a97578e91b949", null ],
    [ "satLimitHigh", "classcontroller__driver_1_1ClosedLoop.html#a11b7946e360a4d906cb3891512a35dfe", null ],
    [ "satLimitLow", "classcontroller__driver_1_1ClosedLoop.html#a0a43b8562407da052a8036cf6624f6d2", null ]
];