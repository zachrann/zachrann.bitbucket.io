var classtouch__driver_1_1TouchPanel =
[
    [ "__init__", "classtouch__driver_1_1TouchPanel.html#a93c4e08107f97139d0fec27df71c4960", null ],
    [ "allScan", "classtouch__driver_1_1TouchPanel.html#a0f3d5c3ea28d3f15a579049897e4fa98", null ],
    [ "readPos_Vel", "classtouch__driver_1_1TouchPanel.html#ae8399c078b1deed42c5400b4251dee2a", null ],
    [ "setCal", "classtouch__driver_1_1TouchPanel.html#af0325f848e1fde0324de26d85ae8ddb9", null ],
    [ "A0", "classtouch__driver_1_1TouchPanel.html#a267428910a9757a8e62f029055e336ee", null ],
    [ "A1", "classtouch__driver_1_1TouchPanel.html#ada209c55aa594e913cc5ad7ccb5b1459", null ],
    [ "A6", "classtouch__driver_1_1TouchPanel.html#a1bb13a7d3aae5b1d010d9f3b717e4d00", null ],
    [ "A7", "classtouch__driver_1_1TouchPanel.html#a589e9f11625d30d28edca27fd359c8cd", null ],
    [ "ADC", "classtouch__driver_1_1TouchPanel.html#a499d340a48b37c11d40dfb5bfc5e4272", null ],
    [ "IN", "classtouch__driver_1_1TouchPanel.html#a935625b107ff286a8f488e0a8376d556", null ],
    [ "inn", "classtouch__driver_1_1TouchPanel.html#a72dfe7a35168f3de6c23b5f1f6651e7d", null ],
    [ "length", "classtouch__driver_1_1TouchPanel.html#ab5909a76ec581d5147993963fc9cbc2a", null ],
    [ "out", "classtouch__driver_1_1TouchPanel.html#afd6dc991f1980a53e4da7eb1d6d08828", null ],
    [ "pin", "classtouch__driver_1_1TouchPanel.html#ad47d5f73722ffeff4caad7bb753e8876", null ],
    [ "setTime", "classtouch__driver_1_1TouchPanel.html#aafb0f0cd42b594191c4bb47725385397", null ],
    [ "wait", "classtouch__driver_1_1TouchPanel.html#accac60a867e0e8140132bd858f900dc3", null ],
    [ "width", "classtouch__driver_1_1TouchPanel.html#a83299ebdb90774dd6e733241bbdca4b4", null ],
    [ "XM", "classtouch__driver_1_1TouchPanel.html#a1a4e5a45ee03041eadacac5236ddb69b", null ],
    [ "XP", "classtouch__driver_1_1TouchPanel.html#aee757d6c662f3a72c4068ce8c9347f71", null ],
    [ "YM", "classtouch__driver_1_1TouchPanel.html#ab95297ce5592ea3bbe72e29dc31c2997", null ],
    [ "YP", "classtouch__driver_1_1TouchPanel.html#aac4c038b5de19c8cd9ff286a75e76eec", null ]
];