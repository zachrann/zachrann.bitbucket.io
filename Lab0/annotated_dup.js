var annotated_dup =
[
    [ "controller_driver", null, [
      [ "ClosedLoop", "classcontroller__driver_1_1ClosedLoop.html", "classcontroller__driver_1_1ClosedLoop" ]
    ] ],
    [ "imu_driver", null, [
      [ "BNO055", "classimu__driver_1_1BNO055.html", "classimu__driver_1_1BNO055" ]
    ] ],
    [ "motor_driver", null, [
      [ "DRV8847", "classmotor__driver_1_1DRV8847.html", "classmotor__driver_1_1DRV8847" ],
      [ "Motor", "classmotor__driver_1_1Motor.html", "classmotor__driver_1_1Motor" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "ShareMotor", "classshares_1_1ShareMotor.html", "classshares_1_1ShareMotor" ]
    ] ],
    [ "task_controller", null, [
      [ "Controller", "classtask__controller_1_1Controller.html", "classtask__controller_1_1Controller" ]
    ] ],
    [ "task_hardware", null, [
      [ "Task_Hardware", "classtask__hardware_1_1Task__Hardware.html", "classtask__hardware_1_1Task__Hardware" ]
    ] ],
    [ "task_imu", null, [
      [ "TaskIMU", "classtask__imu_1_1TaskIMU.html", "classtask__imu_1_1TaskIMU" ]
    ] ],
    [ "task_motorFINAL", null, [
      [ "TaskMotor", "classtask__motorFINAL_1_1TaskMotor.html", "classtask__motorFINAL_1_1TaskMotor" ]
    ] ],
    [ "task_record", null, [
      [ "TaskRecord", "classtask__record_1_1TaskRecord.html", "classtask__record_1_1TaskRecord" ]
    ] ],
    [ "task_touch", null, [
      [ "TouchPanel", "classtask__touch_1_1TouchPanel.html", "classtask__touch_1_1TouchPanel" ]
    ] ],
    [ "task_userFINAL", null, [
      [ "TaskUser", "classtask__userFINAL_1_1TaskUser.html", "classtask__userFINAL_1_1TaskUser" ]
    ] ],
    [ "touch_driver", null, [
      [ "TouchPanel", "classtouch__driver_1_1TouchPanel.html", "classtouch__driver_1_1TouchPanel" ]
    ] ]
];