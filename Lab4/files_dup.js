var files_dup =
[
    [ "controller_driver.py", "controller__driver_8py.html", [
      [ "controller_driver.ClosedLoop", "classcontroller__driver_1_1ClosedLoop.html", "classcontroller__driver_1_1ClosedLoop" ]
    ] ],
    [ "encoder_lab3.py", "encoder__lab3_8py.html", [
      [ "encoder_lab3.Encoder", "classencoder__lab3_1_1Encoder.html", "classencoder__lab3_1_1Encoder" ]
    ] ],
    [ "Menulab4.py", "Menulab4_8py.html", null ],
    [ "motor_driver.py", "motor__driver_8py.html", [
      [ "motor_driver.DRV8847", "classmotor__driver_1_1DRV8847.html", "classmotor__driver_1_1DRV8847" ],
      [ "motor_driver.Motor", "classmotor__driver_1_1Motor.html", "classmotor__driver_1_1Motor" ]
    ] ],
    [ "ResultsLab4.py", "ResultsLab4_8py.html", null ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.ShareMotor", "classshares_1_1ShareMotor.html", "classshares_1_1ShareMotor" ],
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_hardware.py", "task__hardware_8py.html", "task__hardware_8py" ],
    [ "task_userLAB3.py", "task__userLAB3_8py.html", "task__userLAB3_8py" ]
];