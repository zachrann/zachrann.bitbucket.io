var files_dup =
[
    [ "encoder_lab3.py", "encoder__lab3_8py.html", [
      [ "encoder_lab3.Encoder", "classencoder__lab3_1_1Encoder.html", "classencoder__lab3_1_1Encoder" ]
    ] ],
    [ "main_lab3.py", "main__lab3_8py.html", "main__lab3_8py" ],
    [ "Menulab3.py", "Menulab3_8py.html", null ],
    [ "motor_driver.py", "motor__driver_8py.html", [
      [ "motor_driver.DRV8847", "classmotor__driver_1_1DRV8847.html", "classmotor__driver_1_1DRV8847" ],
      [ "motor_driver.Motor", "classmotor__driver_1_1Motor.html", "classmotor__driver_1_1Motor" ]
    ] ],
    [ "ResultsLab3.py", "ResultsLab3_8py.html", null ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.ShareMotor", "classshares_1_1ShareMotor.html", "classshares_1_1ShareMotor" ],
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_encoderLAB3.py", "task__encoderLAB3_8py.html", "task__encoderLAB3_8py" ],
    [ "task_motor.py", "task__motor_8py.html", "task__motor_8py" ],
    [ "task_userLAB3.py", "task__userLAB3_8py.html", "task__userLAB3_8py" ]
];