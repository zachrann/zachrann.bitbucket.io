var annotated_dup =
[
    [ "encoder_lab3", null, [
      [ "Encoder", "classencoder__lab3_1_1Encoder.html", "classencoder__lab3_1_1Encoder" ]
    ] ],
    [ "motor_driver", null, [
      [ "DRV8847", "classmotor__driver_1_1DRV8847.html", "classmotor__driver_1_1DRV8847" ],
      [ "Motor", "classmotor__driver_1_1Motor.html", "classmotor__driver_1_1Motor" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "ShareMotor", "classshares_1_1ShareMotor.html", "classshares_1_1ShareMotor" ]
    ] ],
    [ "task_encoderLAB3", null, [
      [ "TaskEncoder", "classtask__encoderLAB3_1_1TaskEncoder.html", "classtask__encoderLAB3_1_1TaskEncoder" ]
    ] ],
    [ "task_motor", null, [
      [ "TaskMotor", "classtask__motor_1_1TaskMotor.html", "classtask__motor_1_1TaskMotor" ]
    ] ],
    [ "task_userLAB3", null, [
      [ "TaskUser", "classtask__userLAB3_1_1TaskUser.html", "classtask__userLAB3_1_1TaskUser" ]
    ] ]
];